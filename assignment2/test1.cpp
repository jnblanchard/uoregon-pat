// Templated class declaration used in template parameter example code
template <typename T>
class templateClass
   {
     public:
          int x;
          void foo(int);
          void foo(double);
   };

// Overloaded functions for testing overloaded function resolution
void foo(int);

void foo(double) 
	{
	int x = 10;
	int y;
	for (int i =0; i < 4; i++) {
		int x;
	}
	
	if(x)
		y = 5;
	else 
		y = 12;
}

int main()
	{
		foo(3);
		foo(20);
		
		templateClass<char> instantiatedClass;
     	instantiatedClass.foo(7);
     	instantiatedClass.foo(7.0);
	
     	for (int i=0; i < 4; i++) {
          int x;
        }
     return 0;
   }

		