// Templated class declaration used in template parameter example code
template <typename T>
class templateClass
   {
     public:
          int x;
          void foo(int);
          void foo(double);
   };

// Overloaded functions for testing overloaded function resolution
void foo(int);

void foo(double) 
	{
	int x = 5;
	int y;
	for (int i =0; i < 8; i++) {
		int x;
	}
	
	if(x)
		y = 10;
	else 
		y = 2;
}

int main()
	{
		foo(14);
		foo(15);
		
		templateClass<char> instantiatedClass;
     	instantiatedClass.foo(7);
     	instantiatedClass.foo(7.0);
	
     	for (int i=0; i < 8; i++) {
          int x;
        }
     return 0;
   }