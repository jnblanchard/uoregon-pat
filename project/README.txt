Metrics Evaluation by John Blanchard and John Blanchard alone!

USAGE:
1. Use the Makefile: make all   (Reaches into ix-saucy Rose root directories)
2. Execute conditonalFlow: ./conditionalFlow test.cpp
3. Optional; Another test file: ./conditionalFlow testOne.c

Control Flow .dot File Creation:  REFERENCE/CREDIT PAGE 140 ROSE TUTORIAL
Lines 108 through 119 in main create a .dot file of all functions in the input file. The .dot file is visually displayed using GraphVis. For the rest of my metric examination having the visual control flow graph made debugging much easier!

Cyclomatic Complexity: 
This is the direct measurement of the number of linearly independent paths through a program's source code or in this case functions too.
Usually cyclomatic complexity is computed with a control flow graph thus the .dot file visual representation made debugging my Rose methods much easier. I used the visitor traversal class that we've used all term to determine where conditional statements exist then incremented a counter.

Halstead's Metrics:
Halstead's goal was to identify measurable properties of software and the relationships between them. He theorized that these metrics are computed statically from the code.

A = Number of distinct operators
B = Number of distinct operands

Program length = A + B
Volume = (A+B) * log(A+b)

Now here is where Halstead's metrics became observations, he estimated bug count in a program through this computation.

Bug Estimation = Volume / 3000

(Don't ask me where Halstead came up with 3000....)

Thank You Professor Norris!
-John Blanchard


