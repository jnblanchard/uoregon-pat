#include <stdio.h>
#include <string.h>
#include <assert.h>
void foo(char *someStr, char buf[4]) {
   int i = 0, j = 1, k = 2, n = 3;
  
   if( i < 0 || j < 2 ) {
       j++;
   	   if( k < 3 ) {
   	       j++;
   	       if( n < 4 ) {
   	           j++;
   	       }
   	   }
   }
   else {
       k++;
       if( i < 2 ) {
           k++;
       }
   }
   
   if( n < 5 ) {
       n++;
   }
}

int fib(int n) {
	if(n < 2) {
		return 1;
	}
	else {
		return n * fib(n-1);
	}
}

void addTwoTimesN(int n) {
    if( n > -1) {
		n += 2;
	}
	if(n > 0) {
		n *= n;
	}
}

