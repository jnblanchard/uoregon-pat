#include <stdio.h>
void foo(int a[10][10], int *b) {
  for (int j = 1; j < 10; j++) {
    for (int i = 0; i < 10; i++) {
      a[i][j] = 2 * a[i][j-1];
    }
  }
  for (int j = 0; j < 10; j++) {
    for (int i = 0; i < 10; i++) {
      b[j] = b[j] + a[i][j];
    }
  }
}