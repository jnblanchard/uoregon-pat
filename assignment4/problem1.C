// ROSE is a tool for building preprocessors, this file is an example preprocessor built with ROSE.
// rose.C: Example (default) ROSE Preprocessor: used for testing ROSE infrastructure

#include "rose.h"
#include <iostream>
#include <string>
using namespace std;

bool foundPrint = false;
bool safePrint = true;
vector<string> expVector;
int ind = 0;
int line = 0;
char searchItem = '"';
char percent = '%';
string curString;

class visitorTraversal : public AstSimpleProcessing
   {
     public:
          visitorTraversal();
          virtual void visit(SgNode* n);
          virtual void atTraversalEnd();
   };

visitorTraversal::visitorTraversal()
   {
   }

void visitorTraversal::visit(SgNode* n)
   {
   	 if(foundPrint == true && n->class_name() == "SgExprListExp") {
   	 	expVector.push_back(n->unparseToString());
   	 	curString = expVector[ind];
   	 	size_t found = curString.find(searchItem);
   	 	if(found == string::npos) {
   	 		safePrint = false;
   	 	}
   	 	if(safePrint == false) {
   	 		cout << "Suspicious printf at line: " << line-1 << endl;
   	 		safePrint = true;
   	 	}
   	 	ind += 1;
   	 	foundPrint = false;
   	 }
   	 if(n->class_name() == "SgFunctionRefExp" && n->unparseToString() == "printf") {
   	 	SgLocatedNode* someNode = isSgLocatedNode(n->get_parent()->get_parent());
   	 	line = someNode->get_file_info()->get_line();
   	 	foundPrint = true;
   	 }
   }

void visitorTraversal::atTraversalEnd()
   {
     printf ("----------------------------------------------------- \n");
   }

int
main ( int argc, char* argv[] )
   {
     if (SgProject::get_verbose() > 0)
          printf ("In visitorTraversal.C: main() \n");

     SgProject* project = frontend(argc,argv);
     ROSE_ASSERT (project != NULL);

  // Build the traversal object
     visitorTraversal exampleTraversal;

  // Call the traversal function (member function of AstSimpleProcessing)
  // starting at the project node of the AST, using a preorder traversal.
     exampleTraversal.traverseInputFiles(project,preorder);

     return 0;
   }